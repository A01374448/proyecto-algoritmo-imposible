package mx.itesm.util;

import java.util.AbstractList;
import java.util.NoSuchElementException;

public class ArrayList<E> extends AbstractList<E> {

	private int listCapacity;
	private int listSize;
	private E[] data;
	
	@SuppressWarnings("unchecked")
	public ArrayList(E... elements) {
		listCapacity = 10;
		listSize = 0;
		
		data = (E[]) new Object[listCapacity];
		for (E element : elements) {
            add(element);
        }
	}
	
	@Override
	public E get(int index) {
		if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        return data[index];
	}

	@Override
	public int size() {
		return listSize;
	}
	
	public int capacity() {
		return listCapacity;
	}
	
	@Override
    public E set(int index, E element) {
		if(index < 0 || index >= size()) {
			throw new IndexOutOfBoundsException();
		}
		if (element == null) {
			throw new NullPointerException();
		}
		
		E temp = data[index];	
		data[index] = element;
		
		return temp;
		
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public void add(int index, E element) {
    	if(index < 0 || index > size()) {
			throw new IndexOutOfBoundsException();
		}
		if (element == null) {
			throw new NullPointerException();
		}

		E [] temp = (E[]) new Object[listCapacity];
			for (int i= 0; i<=listSize-1; i++) {
				temp[i]=data[i];
			}
		
		if (listSize == listCapacity) {
			
			listCapacity= listCapacity*2;
			data = (E[]) new Object[listCapacity];
			for (int i= 0; i<=listSize-1; i++) {
				data[i]=temp[i];
			}
			temp = (E[]) new Object[listCapacity];
			for (int i= 0; i<=listSize-1; i++) {
				temp[i]=data[i];
				
			}
			
			
		}
			
		for (int i= index; i<=listSize-1;i++) {
			data[i+1] = temp[i];
		}
			
			
		data[index]=element;
			
		listSize++;
			
		
    }

    @Override
    @SuppressWarnings("unchecked")
    public E remove(int index) {
    	if(index < 0 || index >= size()) {
			throw new IndexOutOfBoundsException();
		}
    E [] tempo = (E[]) new Object[listCapacity];
		for (int i= 0; i<=listSize-1; i++) {
			tempo[i]=data[i];
		}	
    	
    E temp = data[index];
    for (int i= index; i<=listSize-1;i++) {
		data[i] = tempo[i+1];
	}
    
    listSize--;
    
    return temp;
    }
    
    public E getFirst() {
		if (size()==0) {
			throw new NoSuchElementException();
		}
		return data[0];
		
	}
	
	public E getLast() {
		if (size()==0) {
			throw new NoSuchElementException();
		}
		return data[listSize-1];
	}
	
	@Override
	public void clear() {
		listCapacity = 10;
		listSize = 0;
		
		data = (E[]) new Object[listCapacity];
	}
	
	public void trimToSize() {
		listCapacity=listSize;
		
		E[] tempData = (E[])new Object[listCapacity];
		for (int i = 0; i<listSize; i++) {
			tempData[i]= data[i];
		}
		
		data = tempData;
	}
}
