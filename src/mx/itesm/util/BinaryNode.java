package mx.itesm.util;

import java.io.Serializable;

public class BinaryNode<E> implements Serializable {
	private static final long serialVersionUID = 1;
	
	
	//Declaramos que las ligas son nodos
	public BinaryNode<E> left;
	public BinaryNode<E> right;
			
	//Declaramos el valor que puede tener el nodo
	public E value;

	//Inicializamos la raiz
	public BinaryNode() {
		value=null;
		left=null;
		right=null;
	}
		
	//Creamos un nuevo nodo
	public BinaryNode(E value) {
		this.value= value;
		right=null;
		left=null;
	}
}
