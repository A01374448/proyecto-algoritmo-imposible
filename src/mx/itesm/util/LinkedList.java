package mx.itesm.util;

import java.util.AbstractList;
import java.util.NoSuchElementException;

public class LinkedList<E> extends AbstractList<E> {
	private Node<E> head;
	private int size;
	
	
	
	public LinkedList() {
		//instanciamos un centinela y declaramos que el tama�o es cero por ende lista vacia
		head= new Node<E>();
		size=0;
	}
	
	@SafeVarargs
	public LinkedList(E... elements) {
		head= new Node<E>();
		size= 0;
		
		for(E element : elements) {
			addLast(element);
		}
	}
	
	public void addFirst(E element) {
		//marcamos que no se pueden agregar elementos que no tengan nada adentro
		if (element == null){
			throw new NullPointerException();
		}
		
		//obtenemos los nodos a modificar
		Node<E> primero= new Node<E>(element);
		Node<E> anteriorNodo= head.next;
		
		//cambiamos las etiquetas del nuevo nodo
		primero.next=anteriorNodo;
		primero.prev=head;
		
		//cambiamos las eticas de los nodos viejos
		head.next=primero;
		anteriorNodo.prev= primero;
		
		//incrementamos el valor de tama�o de la lista
		size++;
	}
	
	public void addLast(E element) {
		//marcamos que no se pueden agregar elementos que no tengan nada adentro
		if (element == null){
				throw new NullPointerException();
			}
			
		//obtenemos los nodos a modificar
		Node<E> ultimo= new Node<E>(element);
		Node<E> anteriorNodo= head.prev;
		
		//cambiamos las etiquetas del nuevo nodo
		ultimo.next=head;
		ultimo.prev=anteriorNodo;
		
		//cambiamos las eticas de los nodos viejos
		head.prev=ultimo;
		anteriorNodo.next= ultimo;
		
		//incrementamos el valor de tama�o de la lista
		size++;
	}
	
	@Override
	public E get(int arg0) {
		Node<E> getter = node (arg0);
		return getter.value;
	}

	@Override
	public int size() {
		return size;
	}

	/**
	 * Gets the node at the specified index
	 * @param index the index of the node to get
	 * @return the node a t the specified position
	 * @throws IndexOutOfBoundsException if the index is out of range
	 * (index < 0 || index >= size())
	 */
	private Node<E> node(int index){
		if ((index < 0 || index >= size())) {
			throw new IndexOutOfBoundsException("Index out of range");
		}
		Node<E> currentNode = head.next;
		for (int i=0; i< index; i++) {
			currentNode= currentNode.next;
		}
		
		return currentNode;
	}
	
	public String toStringReverse() {

		
		Node<E> currentNode= head.prev;
		
		if (currentNode== head) {
			return "[]";
		}
		
		String returnValue = "[" + currentNode.value;
		while (currentNode.prev !=head) {
			currentNode= currentNode.prev;
			returnValue += ", " + currentNode.value;
		
					
		}
		
		returnValue += "]";
		
		return returnValue;
	}

	@Override
	public void add(int index, E element) {
		//marcamos que no se pueden agregar elementos que no tengan nada adentro o que haya indices mas grandes a la lista
		if (element == null){
				throw new NullPointerException();
		}
		if ((index < 0 || index > size())) {
			throw new IndexOutOfBoundsException("Index out of range");
		}
		if(index == size()) {
			addLast(element);
		}
		else {
		//obtenemos los nodos a modificar
		Node<E> currentNode = head.next;
		for (int i=0; i< index; i++) {
			currentNode = currentNode.next;
		}
		Node<E> newNode= new Node<E>(element);{}
		Node<E> prevNode= currentNode.prev;
				
		//cambiamos las etiquetas del nuevo nodo
		newNode.next=currentNode;
		newNode.prev=prevNode;
				
		//cambiamos las eticas de los nodos viejos
		prevNode.next=newNode;
		currentNode.prev= newNode;
				
		//incrementamos el valor de tama�o de la lista
		size++;
		}
	}
	
	@Override
	public E set(int index, E element) {
		//marcamos que no se pueden agregar elementos que no tengan nada adentro o que haya indices mas grandes a la lista
		if (element == null){
			throw new NullPointerException();
		}
		if ((index < 0 || index >= size())) {
			throw new IndexOutOfBoundsException("Index out of range");
		}
				
		//obtenemos los nodos a modificar
		Node<E> currentNode = head.next;
		for (int i=0; i< index; i++) {
			currentNode= currentNode.next;
		}
		
		//guardamos el dato, lo cambiamos y regresamos lo cambiado
		E currentValue = currentNode.value;
		currentNode.value = element;
		
		
		return currentValue;
	}
	
	/**
	 * Removes and returns the first element from this list.
	 * @return the first element from this list
	 * @throws NoSuchElementException if this list is empty
	 */
	public E removeFirst() {
		//marcamos que no se pueden agregar elementos que no tengan nada adentro
		if (size == 0 || head.next==head){
			throw new NoSuchElementException();
		}
				
		//obtenemos los nodos a modificar
		Node<E> slashedNode= head.next;
		Node<E> newFirstNode= slashedNode.next;
				
		//cambiamos las etiquetas del nodo a eleminar
		slashedNode.next=slashedNode;
		slashedNode.prev=slashedNode;
				
		//cambiamos las eticas de los nodos viejos
		head.next=newFirstNode;
		newFirstNode.prev= head;
				
		//reducimos el valor de tama�o de la lista
		size--;
				
		return slashedNode.value;
	}
	
	/**
	 * Removes and returns the last element from this list.
	 * @return the last element from this list
	 * @throws NoSuchElementException if this list is empty
	 */
	public E removeLast() {
		//marcamos que no se pueden agregar elementos que no tengan nada adentro
		if (size == 0 || head.next==head){
			throw new NoSuchElementException();
		}
				
		//obtenemos los nodos a modificar
		Node<E> slashedNode= head.prev;
		Node<E> newFirstNode= slashedNode.prev;
				
		//cambiamos las etiquetas del nodo a eleminar
		slashedNode.next=slashedNode;
		slashedNode.prev=slashedNode;
				
		//cambiamos las eticas de los nodos viejos
		head.prev=newFirstNode;
		newFirstNode.next= head;
				
		//reducimos el valor de tama�o de la lista
		size--;
				
		return slashedNode.value;
	}
	
	@Override
	public E remove(int index) {
		//marcamos que no haya indices mas grandes a la lista
		if ((index < 0 || index >= size())) {
			throw new IndexOutOfBoundsException("Index out of range");
		}
		
		//obtenemos los nodos a modificar
		Node<E> slashedNode = head.next;
		for (int i=0; i< index; i++) {
			slashedNode= slashedNode.next;
		}
		Node<E> nextNode= slashedNode.next;
		Node<E> prevNode= slashedNode.prev;
		
				
		//cambiamos las etiquetas del nuevo nodo
		slashedNode.next=slashedNode;
		slashedNode.prev=slashedNode;
				
		//cambiamos las eticas de los nodos viejos
		nextNode.prev=prevNode;
		prevNode.next= nextNode;
				
		//reducimos el valor de tama�o de la lista
		size--;

		return slashedNode.value;
	}
	
	public E getFirst() {
		//marcamos que no se pueden agregar elementos que no tengan nada adentro
		if (size == 0 || head.next==head){
			throw new NoSuchElementException();
		}
		Node<E> getter= head.next;
		
		return getter.value;
		
	}
	
	public E getLast() {
		//marcamos que no se pueden agregar elementos que no tengan nada adentro
		if (size == 0 || head.next==head){
			throw new NoSuchElementException();
		}
		Node<E> getter= head.prev;
		
		return getter.value;
		
	}
	
}
