package mx.itesm.exercises;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Exam2 {

		public static int max(List<Integer> list) {
			if (list.isEmpty()) {
				return Integer.MIN_VALUE;
			}else {
				int prim =list.get(0);
				int temp;
				List<Integer> tempL = new LinkedList<Integer>(list);
					tempL.remove(0);
					temp = max(tempL);
				if (prim<temp) {
					return temp;
				}
				else return prim;
			}
			
		}
		
		public static int maxSymbolDepth(String expr, char opening, char closing) {
			if(opening == closing){
				return -1;
			}else {
				//hacemos una pila
				Deque<Character> stack = new LinkedList<Character>();
				//hacemos el contador
				int cont = 0;
				int max =  0;
				
				//recorremos cada elemento de la lista
				for (int i =0; i<= expr.length()-1; i++) {
					//tomamos el simbolo donde estamos como string
					 char symbol =  expr.charAt(i);
					
					//checamos si abre o cierra
					if (symbol == (opening)  )  {
						//lo agregamos a la lista
						stack.push(symbol);
					}
					else if (symbol == (closing) )  {
						//tiene que haber un simbolo con le cual comparar
						if(stack.isEmpty()) {
							return -1;
						}
						//checamos si corresponden los valores
						else {
							stack.pop();
							cont +=1;
						}
					}
					if (stack.isEmpty()) {
						if (cont > max) {
							max = cont;
							cont = 0;
						}
					}
					
				}
				if (!stack.isEmpty()) {
					return -1;
				}
				return max;
			}
		}
		
		public static String toBinary(int n) {
			if (n <= 0) {
				return "";
			}else {
				String Sres;
				String temp;
				int var = n/2;
				int res = n%2;
				temp = toBinary(var);
				Sres = temp + Integer.toString(res);
				return Sres;
				
				
			}
		}
	}

	

