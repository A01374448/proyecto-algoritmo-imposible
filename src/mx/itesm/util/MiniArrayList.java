package mx.itesm.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MiniArrayList<E> implements Iterable<E>{
	
	private class MiniArrayListIterator implements Iterator<E>{
		private int index = 0;
		
		@Override
		public boolean hasNext() {
			return index < elements.length;
		}
		
		@Override
		public E next() {
			if(!hasNext()) {
				throw new NoSuchElementException();
			}
			
			return elements[index++];
		}
	}
	
	private E[] elements;
	
	@SafeVarargs
	public MiniArrayList (E... collection) {
		elements = collection;
	}

	public E get (int index) {
		if (index <0 || index >= elements.length) {
			throw new IndexOutOfBoundsException();
		}
		
		return elements[index];
	}
	
	public int size() {
		return elements.length;
		
	}
	
	@Override
	public Iterator<E> iterator(){
		return new MiniArrayListIterator();
	}
	
	public static void main(String[] args) {
		MiniArrayList<Integer> list = new MiniArrayList<Integer>(4, 8, 15, 16, 23, 42 );
		
		//Visitamos todos los elementos de list usando un iterador explicito
		Iterator<Integer> iterator = list.iterator();
		while (iterator.hasNext()) {
			Integer element = iterator.next();
			System.out.print(element + " ");
		}
		System.out.println();
		//Visitamos todos los elementos de list usando un
		for (Integer element : list) {
			System.out.print(element + " ");
		}
	}
	
}
