package mx.itesm.exercises;

import mx.itesm.util.LinkedList;
import java.util.Random;

public class LinkedListApplications {
	

	
	public static boolean isPalindrome(String word) {
		//creamos una lista ligada y un marcador
		boolean result= true;
		LinkedList<Character> palindrome = new LinkedList<Character>();
		
		//solo comparamos letras no mayusculas
		word = word.toLowerCase();
		
		//metemos cada caracter como una entrada en la lista
		for(int i=0; i<=word.length()-1; i++) {
			
			palindrome.addLast(word.charAt(i));
		}
		
		
		//comparamos el primero con el ultimo y los eliminamos
		while (palindrome.size()>=2) {
			char next = palindrome.getFirst();
			char last = palindrome.getLast();
			
			if(next == last) {
				palindrome.removeFirst();
				palindrome.removeLast();
			}
			else {
				result = false;
				break;
			}
			
			
		}
		return result;
	}
	
	public static LinkedList<Character> jumbleLetters(String word) {
		//creamos la lista, un boolean y el RNG
		LinkedList<Character> scrable = new LinkedList<Character>();
		Random generator = new Random(5);
		boolean RNG;
		
		//mezclamos las palabras
		for(int i=0; i<=word.length()-1; i++) {
			RNG = generator.nextBoolean();
			if(RNG == true) {
				scrable.addLast(word.charAt(i));
			}
			else{
				scrable.addFirst(word.charAt(i));
			}
		}
		
		
		return scrable;
	}

	public static String sortingHat(LinkedList<String> students, int n) {
		//comprobamos los parametros
		if(n<=1 | n>=8 | students.size()==0) {
			throw new IllegalArgumentException();
			}
		int count = 0;
		//mientras la lista tenga mas de un elemento
		while(students.size()!=1) {
			//reiniciamos la variable desde el lugar donde se quedo
			count = n + count;
			
				//el tama�o de n tiene que ser menor o igual a la lista
				while(students.size()<count) {
					count -= students.size();
				}
				
				//si son del mismo tama�o se elimina el ultimo
				if(students.size()==count) {
					students.removeLast();
					count = 0;
				}
				//si la lista es mas grande se elimina el del contador
				else if(students.size()>count) {
					count--;
					students.remove(count);
				}
		}
		
		return students.getFirst();
	}
}
