package mx.itesm.util;

import java.util.Random;
import java.util.Deque;
import java.util.LinkedList;

public class Arrays {
	
	
	
	public static int findMinimum(int array[]) {
		int min;
	
		if (array.length != 0) {
			min=array[0];
			
		for (int i = 0; i<=array.length-1; i++) {
			if (array[i]<min) {
				min= array[i];
			}
		}
			return min;
		}
		
		
		return Integer.MIN_VALUE;
	}
	
	public static int findMaximum(int array[]){
	int max;
		
		if (array.length != 0) {
			max=array[0];
			
		for (int i = 0; i<=array.length-1; i++) {
			if (array[i]>max) {
				max= array[i];
			}
		}
			return max;
		}
		
		
		return Integer.MIN_VALUE;
	}

	public static float findAverage(int array[]) {
	float ave=0;
		
		if (array.length != 0) {
		
		for (int i = 0; i<=array.length-1; i++) {
			ave += array[i];
		}
			ave= ave/array.length;
			return ave;
		}
		
		
		return Integer.MIN_VALUE;
	}

	public static float sampleVariance(int array[]) {
	float var=0;

		
		if (array.length != 0) {
		float ave= findAverage(array);
		
		for (int i = 0; i<=array.length-1; i++) {
			var+=Math.pow((array[i]-ave), 2);
		}
			var= var/array.length;
			return var;
		}
		
		
		return Integer.MIN_VALUE;
	}

	public static String toString(int[] array) {
	String finale="[";

		
		if (array.length != 0) {
		
		for (int i = 0; i<=array.length-1; i++) {
			if (array.length-1 != i) {
				finale += array[i]+ ", ";
			}else finale += array[i];
		}
			finale += "]";
			return finale;
		}
		
		
		return "[]";
	}
	
	public static void reverse(int[] array) {
		
		int temp;
		
		if (array.length != 0) {
			for (int i=0; i<=(array.length-1)/2; i++) {
				temp = array[i];
				array[i]=array[array.length-1-i];
				array[array.length-i-1]= temp;
			}
		}
		
		
	}
	
	public static int indexOfLinear(int[] array, int target) {
		
		for (int i=0; i<array.length; i++ ) {
			if(array[i]== target) {
				return i;
			}
		}
		
		return -1;
	}
	
	public static int indexOfBinary(int[] array, int target) {
		int min = 0;
		int max = array.length-1;
		
		
		while (min <= max) {
			int mid = (min + max)/2;
			
			if (target < array[mid]) {
				max = mid -1;
				
			}
			else if (target > array[mid]) {
				min = mid +1;
			}
			else if (target == array[mid]) {
				return mid;
				
			}
			
		}
		
		
		return -1;
	}
	
	public static void insertionsort(int[] unorderedArray) {
		for (int i = 0; i<unorderedArray.length; i++) {
			for (int k=i; k>0; k--) {
				if (unorderedArray[k] < unorderedArray[k-1]) {
					int temp = unorderedArray[k-1];
					 unorderedArray[k-1] =  unorderedArray[k];
					 unorderedArray[k] = temp;
				}
			}
		}
	}
	
	public static void selectionsort(int[] unorderedArray) {
		for (int i = 0; i<=unorderedArray.length -1; i++) {
			int temp = unorderedArray[i];
			int min = i;
			for (int k = i; k<=unorderedArray.length -1; k++) {
				if (temp > unorderedArray[k]) {
					temp=unorderedArray[k];
					min= k;
					}
			}
			
			unorderedArray[min] = unorderedArray[i];
			unorderedArray[i] = temp;
			
		}
	}
	
	public static void bubblesort(int[] unorderedArray) {
		boolean notSorted = true;
		while (notSorted) {
			notSorted = false;
			
			for (int i = 1; i<=unorderedArray.length -1; i++) {
				if (unorderedArray[i]<unorderedArray[i-1]) {
					int temp = unorderedArray[i];
					unorderedArray[i] = unorderedArray[i-1];
					unorderedArray[i-1] = temp;
					notSorted=true;
				}
			}
		}
		
	}
	
	public static void quicksort(int[] unorderedArray, int minIndex, int maxIndex) {
		if (minIndex>=maxIndex ) {	
			return;
		}else {
			//escogemos el elemento de en medio
			int mid = minIndex;
			int div= unorderedArray[mid];
			
			//hacemos una pila
			Deque<Integer> before = new LinkedList<Integer>();
			Deque<Integer> after = new LinkedList<Integer>();
		
			for (int i = minIndex+1; i<=maxIndex; i++) {
				if (unorderedArray[i]<div) {
					before.push(unorderedArray[i]);
				}
				else {
					after.push(unorderedArray[i]);
				}
			}
				mid = minIndex+before.size();
				int i=minIndex;
				while ( i<mid && !before.isEmpty()) {
					unorderedArray[i]=before.pop();
					i++;
				}
				i = mid +1;
				while ( i<=maxIndex  && !after.isEmpty()) {
					unorderedArray[i]=after.pop();
					i++;
				}
				unorderedArray[mid]=div;
				
					quicksort(unorderedArray, minIndex,mid-1);
					quicksort(unorderedArray, mid+1,maxIndex);
				
			
		}
	}
	
	public static void mergesort(int[] unorderedArray, int[] scratch, int minIndex, int maxIndex){
		if (minIndex>=maxIndex) {
			return;
		}else {
			int midIndex = (minIndex + maxIndex)/2;
			 mergesort(unorderedArray, scratch,  minIndex,  midIndex);
			 mergesort( unorderedArray,  scratch,  midIndex+1,  maxIndex);
			 
			 int leftIndex =minIndex;
			 int rightIndex= midIndex+1;
			 int j = minIndex;
			 while (leftIndex <=midIndex && rightIndex<=maxIndex) {
				 if (unorderedArray[leftIndex]<unorderedArray[rightIndex]) {
					 scratch[j++]= unorderedArray[leftIndex++];
				 }else {
					 scratch[j++]= unorderedArray[rightIndex++];
				 }
			 }
			 
			 for(int i= leftIndex; i <=midIndex; i++) {
				 scratch[j++]= unorderedArray[i];
			 }
			 for(int k= rightIndex; k <=maxIndex; k++) {
				 scratch[j++]= unorderedArray[k];
			 }
			 
			 for(int i = minIndex; i<=maxIndex; i++) {
				unorderedArray[i]=scratch[i]; 
			 }
			
		}
	}
		
	
}