package mx.itesm.util;

import java.util.List;
import java.util.LinkedList;
import java.util.Collections;
import java.util.Deque;

public class Sorting {
	
	public static <T extends Comparable<T>> List<T> insertionsort(List<T> unorderedList){
		
		//Copiamos los valores de unorderedList y los guardamos en una nueva lista.
		
		List<T> unorderedSection = new LinkedList<T>(unorderedList);
		List<T> orderedSection = new LinkedList<T>();
		
		while(!unorderedSection.isEmpty()) {
			T element = unorderedSection.remove(0);
			
			int index = 0;
			while (index < orderedSection.size() &&
					element.compareTo(orderedSection.get(index))>=0){
				
				
				index ++;
			}
			
			orderedSection.add(index, element);
			
		}
		
		
		return orderedSection;
	}
	
	public static <T extends Comparable<T>> List<T> selectionsort(List<T> unorderedList) {
		List<T> unorderedSection = new LinkedList<T>(unorderedList);
		List<T> orderedSection = new LinkedList<T>();
		
		while (!unorderedSection.isEmpty()) {
			T temp = unorderedSection.get(0);
			int index = 0;
			for (int i = 0; i<unorderedSection.size(); i++ ) {
				if (temp.compareTo(unorderedSection.get(i))>=0){
					temp = unorderedSection.get(i);
					index = i;
				}
			}
			orderedSection.add(orderedSection.size(),unorderedSection.remove(index));
			
		
		}
		return orderedSection;
		
	}

	@SuppressWarnings("rawtypes")
	public static List<Integer> bucketsort(List<Integer> unorderedList) {
		
		List<List> BigList = new LinkedList<List>();
		
		for (int i = 0; i<10; i++) {
			List<Integer> smallList = new LinkedList<Integer>();
			BigList.add(smallList);
		}
		
		for (int i = 0; i<unorderedList.size(); i++) {
			if (unorderedList.get(i) <10) {
				BigList.get(0).add(unorderedList.get(i));
			}else {
				int k = unorderedList.get(i)/10;
				BigList.get(k).add(unorderedList.get(i));
			}
		}
		
		for (int i = 0; i<10; i++) {
			Collections.sort(BigList.get(i));
		}
		List<Integer> FinalList = new LinkedList<Integer>();
		
		for (int i = 0; i<10; i++) {
			List<Integer> TempList = new LinkedList<Integer>(BigList.get(i));
			while(!TempList.isEmpty()) {
				FinalList.add(TempList.remove(0));
			}
		}
		return FinalList;
	}

	public static <T extends Comparable<T>> List<T> bogosort(List<T> unorderedList) {
		if(!unorderedList.isEmpty()) {
			
		List<T> unorderedSection = new LinkedList<T>(unorderedList);
		boolean finish= false;
		
		while (finish == false) {
			Collections.shuffle(unorderedSection);
			for (int i =0; i<unorderedSection.size()-1; i++) {
			if (unorderedSection.get(i).compareTo(unorderedSection.get(i+1))>=0) {
				finish = false;
				break;
			}
			finish = true;
			}
			
		}
		return unorderedSection;
		}
		else {
			return unorderedList;
		}
	}

	public static <T extends Comparable<T>> List<T> quicksort(List<T> unorderedList) {
		if (unorderedList.isEmpty() ) {	
			return unorderedList;
		}else {
			//escogemos el elemento de en medio
			int mid = 0;
			T div= unorderedList.get(0);
			
			//hacemos una pila
			List<T> before = new LinkedList<T>();
			List<T> after = new LinkedList<T>();
		
			for (int i = 1; i<unorderedList.size(); i++) {
				if (div.compareTo(unorderedList.get(i))>=0) {
					before.add(unorderedList.get(i));
				}
				else {
					after.add(unorderedList.get(i));
				}
			}
				
				List<T> ListBefore=  new LinkedList<T>(quicksort(before));
				List<T> ListAfter =  new LinkedList<T>(quicksort(after));
			
	
				List<T> TempList =  new LinkedList<T>();
						
				while (!ListBefore.isEmpty()) {
					TempList.add(ListBefore.remove(0));
				}
				TempList.add(div);
				while (!ListAfter.isEmpty()) {
					TempList.add(ListAfter.remove(0));
				}
				return TempList;
			
		}
	}
	

}
