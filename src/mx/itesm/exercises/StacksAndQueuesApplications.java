package mx.itesm.exercises;

import java.util.Queue;
import java.util.Deque;
import java.util.LinkedList;


public class StacksAndQueuesApplications {
	
	
	
	public static boolean balancedBrackets(String expr) {
		//hacemos una pila
		Deque<String> stack = new LinkedList<String>();
		
		//recorremos cada elemento de la lista
		for (int i =0; i<= expr.length()-1; i++) {
			//tomamos el simbolo donde estamos como string
			 String symbol = "" + expr.charAt(i);
			
			//checamos si abre o cierra
			if (symbol.equals("(") || symbol.equals("{") || symbol.equals("[")   )  {
				//lo agregamos a la lista
				stack.push(symbol);
			}
			else if (symbol.equals(")") )  {
				//tiene que haber un simbolo con le cual comparar
				if(stack.isEmpty()) {
					return false;
				}
				//checamos si corresponden los valores
				if (stack.peek().equals("(")){
					stack.pop();
				}
				else {
					//si no corresponden es false
					return false;
				}
			}
			else if (symbol.equals("}") )  {
				if(stack.isEmpty()) {
					return false;
				}
				if (stack.peek().equals("{")){
					stack.pop();
				}
				else {
					return false;
				}
			}
			else if (symbol.equals("]") )  {
				if(stack.isEmpty()) {
					return false;
				}
				if (stack.peek().equals("[")){
					stack.pop();
				}
				else {
					return false;
				}
			}
			
		}
		
		//checamos que no quede ningun operador abierto
		if (stack.isEmpty()) {
		return true;
		}
		else {
			return false;
		}
	}

	public static Queue<Integer> merge(Queue<Integer> q1, Queue<Integer> q2) {
		//creamos una fila
		Queue<Integer> queue = new LinkedList<Integer>();
		
		//mientras una tenga elementos continuamos agregando
		while(!q1.isEmpty() && !q2.isEmpty()) {
			//agregamos el elmento menor
			if(q1.peek()<q2.peek()) {
				queue.add(q1.remove());
			}
			else {
				queue.add(q2.remove());
			}
		}
		//agregamos toda la lista que quede llena
		if (q1.isEmpty()) {
			while(!q2.isEmpty()) {
				queue.add(q2.remove());
			}
		}
		else if (q2.isEmpty()) {
			while(!q1.isEmpty()) {
				queue.add(q1.remove());
			}
		}
		
		return queue;
	}

	public static int postfixEvaluation(String expr) {
		//creamos una pila para hacer le algoritmo y una fila para tener los elementos a revisar
		Queue<String> elementos = new LinkedList<String>();
		Deque<Integer> deck = new LinkedList<Integer>();
		elementos= tokenize(expr);

		//mientras queden elementos a revisar
		while(!elementos.isEmpty()) {
			//obtenemos el valor a evaluar
			String elemento = elementos.remove();
			
			//si es entero lo agregamos a la pila
			if (Character.isDigit(elemento.charAt(0)) ) {
				deck.push(Integer.parseInt(elemento));
			}
			//de otro modo revisamos que operadores es
			else if (elemento.equals("*")) {
				if (deck.size()>=2) {
					int B = deck.pop();
					int A = deck.pop();
					int result = A*B;
					deck.push(result);
					
				}
				//tiene que haber dos elementos en la pila minimo
				else {
					throw new IllegalArgumentException();
				}
			}
			else if (elemento.equals("/")) {
				if (deck.size()>=2) {
					int B = deck.pop();
					int A = deck.pop();
					int result = A/B;
					deck.push(result);
					
				}
				else {
					throw new IllegalArgumentException();
				}
			}
			else if (elemento.equals("+")) {
				if (deck.size()>=2) {
					int B = deck.pop();
					int A = deck.pop();
					int result = A+B;
					deck.push(result);
					
				}
				else {
					throw new IllegalArgumentException();
				}
			}
			else if (elemento.equals("-")) {
				if (deck.size()>=2) {
					int B = deck.pop();
					int A = deck.pop();
					int result = A-B;
					deck.push(result);
					
				}
				else {
					throw new IllegalArgumentException();
				}
			}
			else {
				throw new IllegalArgumentException();
			}
			
		}
		
		//la pila solo puede tener un elemento al final
		if (deck.size()==1) {
			return deck.pop();
		}
		else {
			throw new IllegalArgumentException();
		}
		
	}

	public static String convertInfixToPostfix(String expr) {
		//creamos una fila para tener los elementos a revisar
		Queue<String> elementos = new LinkedList<String>();
		elementos= tokenize(expr);
		
		//creamos una pila y una fila para hacer el algoritmo
		Deque<String> deck = new LinkedList<String>();
		Queue<String> lineup = new LinkedList<String>();
		
		//hasta que no haya elementos a evaluar
		while (!elementos.isEmpty()) {
			//obtenemos el valor a evaluar
			String elemento = elementos.remove();
			
			//si es entero lo agregamos a la fila
			if (Character.isDigit(elemento.charAt(0)) ) {
				lineup.add(elemento);
			}
			//si es parentesis lo agregamos a la pila
			else if(elemento.equals("(")) {
				deck.push(elemento);
			}
			//si es un operador
			else if(elemento.equals("+") || elemento.equals("-") || elemento.equals("*") || elemento.equals("/") ) {
				//revisamos si la pila esta vacia y hay otro operador antes que este
				while (!deck.isEmpty()){
						if(!deck.peek().equals("(")) {
						//si tiene mayo precedencia lo agregamos a la fila
						if (hasHigherPrecedence(deck.peek(), elemento)) {
							lineup.add(deck.pop());
						}
						else {
							break;
						}
					}
					else {
						break;
					}
				}
				//siempre se agrega el operador a la pila
				deck.push(elemento);
			}
			//parentesis cerrado
			else if(elemento.equals(")")) {
				//se agregan todos los elementos hasta encontrar un parentesis
				while (!deck.isEmpty() ) {
					if (!deck.peek().equals("(")) {
						lineup.add(deck.pop());
					}
					else {
						break;
					}
				}
				//se quita el parentesis
				deck.pop();
			}
			
			
		}
		
		//se agrega lo que quede a la fila
		while(!deck.isEmpty()) {
			lineup.add(deck.pop());
		}

		String resultado = lineup.remove();
		while (!lineup.isEmpty()) {
			resultado = resultado + " " + lineup.remove();
		}
		
				
		return resultado;
	}

	public static java.util.Queue<String> tokenize(String in) {
		java.util.regex.Pattern p = java.util.regex.Pattern.compile("(\\s)|(\\d+)|(.)");
		java.util.regex.Matcher m = p.matcher(in);
		java.util.Queue<String> result = new java.util.LinkedList<String>();
		while (m.find()) {
			if (m.group(1) == null) {
				result.add(m.group());
			}
		}
		return result;
	}

	public static boolean hasHigherPrecedence(String stackTop, String operator) {
		return !((stackTop.equals("+") || stackTop.equals("-")) && (operator.equals("*") || operator.equals("/")));
	}
}
