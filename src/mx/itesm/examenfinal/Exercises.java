package mx.itesm.examenfinal;

import java.util.LinkedList;
import java.util.Queue;

import mx.itesm.util.BinaryNode;

public class Exercises {

	public static <E> E[] completeTreeToArray(BinaryNode<E> node, int treeSize) {
		E[] finalList = (E[]) new Object[treeSize];
		Queue<BinaryNode<E>> hijos = new LinkedList<BinaryNode<E>>();
		hijos.add(node);
		BinaryNode<E> temp;
		int i =0;
		
		while (!hijos.isEmpty()) {
			temp = hijos.remove();
			
			finalList[i++] = temp.value;
			
			if (temp.left != null) {
				hijos.add(temp.left);
			}
			if (temp.right != null) {
				hijos.add(temp.right);
			}
		}
		
		return finalList;
		
	}
	
}
