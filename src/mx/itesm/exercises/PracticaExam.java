package mx.itesm.exercises;

public class PracticaExam {
	
	public static int pow (int base, int exponent) {
		if (exponent == 0) {
			return 1;
		}else {
			int temp = base*pow(base, exponent-1);
			return temp;
		}
	}

}
