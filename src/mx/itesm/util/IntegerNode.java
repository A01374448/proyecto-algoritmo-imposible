package mx.itesm.util;

public class IntegerNode {

	public int value;
	public IntegerNode next;
	public IntegerNode prev;
	
	
	//Crear nodo sentinela
	public IntegerNode() {
		value=-1;
		next=this;
		prev=this;
				
	}
	
	//Crear nodo
	public IntegerNode(int value) {
		this.value= value;
		next = this;
		prev= this;
	}
}
