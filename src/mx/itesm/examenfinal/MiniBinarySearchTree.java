package mx.itesm.examenfinal;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

import mx.itesm.util.BinaryNode;

public class MiniBinarySearchTree<E extends Comparable<E>> extends AbstractSet<E> {

	private BinaryNode<E> root = null;
	private int size = 0;

	@Override
	public Iterator<E> iterator() {
		List<E> result = new LinkedList<E>();
		inorderTraversal(root, result);
		return result.iterator();
	}

	@Override
	public int size() {
		return size;
	}
	
	public int height() {
		return height(root);
	}
	
	public int diameter() {
		return diameter(root);
	}
	
	public boolean isFull() {
		return isFull(root);
	}

	/**
	 * Adds a new element to this binary search tree.
	 * If root is null, then element becomes the tree's root.
	 * If root is not null, then the tree is traversed comparing values.
	 * When a leaf is reached, a new node is inserted.
	 */
	public boolean add(E element) {
		if (root == null) {
			root = new BinaryNode<E>(element);
			size++;
			return true;
		} else {
			BinaryNode<E> currentNode = root;
			while (true) {
				if (element.compareTo(currentNode.value) == 0) {
					return false;
				} else if (element.compareTo(currentNode.value) < 0) {
					if (currentNode.left == null) {
						currentNode.left = new BinaryNode<E>(element);
						size++;
						return true;
					} else {
						currentNode = currentNode.left;
					}
				} else {
					if (currentNode.right == null) {
						currentNode.right = new BinaryNode<E>(element);
						size++;
						return true;
					} else {
						currentNode = currentNode.right;
					}
				}
			}
		}
	}
	
	private void inorderTraversal(BinaryNode<E> node, List<E> result) {
		if (node == null) {
			return;
		}else {
			inorderTraversal(node.left, result);
			result.add(node.value);
			inorderTraversal(node.right, result);
		}
		
			}
	
	private int height(BinaryNode<E> node) {
		if (node == null) {
			return 0;
		}
		else {
			int izq =height(node.left);
			int der =height(node.right);
			int max = Math.max(izq, der) + 1;
			return max;
			
		}
	}

	private int diameter(BinaryNode<E> node) {
		if (node == null) {
			return 0;
		}else{
			int izq = diameter(node.left);
			int der = diameter(node.right);
			int altura = height(node.left) + height(node.right)+1;
			int max= Math.max(izq, der);
			max = Math.max(max, altura);
			return max;
		}
	}
	
	private boolean isFull(BinaryNode<E> node) {
		if (node == null) {
			return true;
		}
		else if (node.left == null && node.right==null) {
			return true;
		}else if(node.right == null) {
			return false;
		}else if(node.left == null) {
			return false;
		}
		else {
			boolean izq= isFull(node.left);
			boolean der= isFull(node.right);
			if (izq== true && der == true) {
				return true;
			}else {
				return false;
			}
		}
	}

}