package mx.itesm.util;

public class Node<T> {
		//Declaramos que las ligas son nodos
		public Node<T> next;
		public Node<T> prev;
		
		//Declaramos el valor que puede tener el nodo
		public T value;
		
		//Inicializamos el sentinela
		public Node() {
			value=null;
			next=this;
			prev=this;
		
		}
		
		//Creamos un nuevo nodo
		public Node(T value) {
			this.value= value;
			next=this;
			prev=this;
		}
		
			

}