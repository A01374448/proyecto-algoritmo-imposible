package mx.itesm.exercises;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Scanner;
import mx.itesm.util.BinaryNode;;

public class AnimalGame implements Serializable {
	
	private static final long serialVersionUID = 1;
	public BinaryNode<String> root;
	Scanner reader = new Scanner(System.in);
	
	public AnimalGame(){
		
		
		
		try (FileInputStream fis = new FileInputStream("datos.bin");
		ObjectInputStream ois = new ObjectInputStream(fis)) {
		// Se requiere hacer un cast porque readObject() regresa un Object.
		root = (BinaryNode<String>) ois.readObject();
		} catch (Exception e) {
		root = new BinaryNode<String>("¿Es un animal domestico?");
		root.left=new BinaryNode<String>("Perro");
		root.right=new BinaryNode<String>("Cocodrilo");
	
		}
		game();
		
		
	}
	
	public void game() {
		
		BinaryNode<String> CurrentNode = root;
		BinaryNode<String> FatherNode = root;
		String value = CurrentNode.value;
		while (value.charAt(0) == '�') {
			System.out.print(value);
			System.out.println(" (y/n):");
			String resp = reader.nextLine();
			if (resp.equals("y")) {
				FatherNode = CurrentNode; 
				CurrentNode = CurrentNode.left;
				value = CurrentNode.value;
			}else if(resp.equals("n")) {
				FatherNode = CurrentNode;
				CurrentNode = CurrentNode.right;
				value = CurrentNode.value;
			}else {
				System.out.println("Respuesta invalida");
			}
			
		}
		boolean bucle = true;
		while (bucle) {
		System.out.print("�Es un (una) " + value +"?");
		System.out.println(" (y/n):");
		String resp = reader.nextLine();
		
		if (resp.equals("y")) {
			System.out.println("���Adiviné!!!");
			bucle = false;
			end();
			
		}else if(resp.equals("n")) {
			bucle = false;
			add(FatherNode, CurrentNode);
		}else System.out.println("Respuesta invalida");
		}
	}
	
	public void end() {
		
		String resp;
		System.out.println("�Deseas jugar otra vez? (y/n): ");
		resp = reader.nextLine();
		if (resp.equals("y")) {
			game();
		}else if(resp.equals("n")) {
			reader.close();
			System.out.println("Adi�s.");
			
			try (FileOutputStream fos = new FileOutputStream("datos.bin");
					ObjectOutputStream oos = new ObjectOutputStream(fos)) {
					// Objeto que se desea hacer persistente.
					oos.writeObject(root);
					} catch (Exception e) {
					e.printStackTrace();    
					}
			
		}else {
			System.out.println("Respuesta no valida");
			end();
		}
	}
	
	public void add(BinaryNode<String> padre, BinaryNode<String> hijo) {
		String animal;
		String respuesta;
		System.out.println("* Escribe el nombre del animal que pensaste: ");
		animal = reader.nextLine();
		System.out.println(" Escribe una afirmaci�n que sea verdad para un (una) " + animal + " pero que sea falsa para un (una) "+ hijo.value);
		respuesta = reader.nextLine();
		BinaryNode<String> animalNode = new BinaryNode<String>(animal); 
		BinaryNode<String> questionNode = new BinaryNode<String>("�"+ respuesta + "?");
		questionNode.left = animalNode;
		if(padre.left.equals(hijo)) {
			padre.left= questionNode;
			questionNode.right= hijo;
			end();
		}else {
			padre.right= questionNode;
			questionNode.right= hijo;
			end();
		}
	}
}
